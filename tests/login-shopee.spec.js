// example.spec.js
const { test, expect } = require('@playwright/test');
const { LoginShopeePage } = require('../pages/login-shopee-page');

test('user fill login field with invalid value', async({page}) =>{
  const loginDev = new LoginShopeePage(page);
  await loginDev.goto();
  await loginDev.home();
  await loginDev.userGoToLoginPage();
  await loginDev.filledLoginPage("albertsimanjuntak02@gmail.com","juntak123");
  await loginDev.validateErrorMessage();
});





