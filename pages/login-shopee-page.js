// playwright-dev-page.js
const { expect } = require('@playwright/test');
const { use } = require('../playwright.config');

exports.LoginShopeePage = class LoginShopeePage {

  /**
   * @param {import('@playwright/test').Page} page
   */
  constructor(page) {
    this.page = page;
  }

  //page locators:
  async inputUsername() { return $('[placeholder="No\\. Handphone\\/Username\\/Email"]') };
  async inputPassword() { return $('[placeholder="Password"]') };
  get loginBtn() { return $('#btnLogin') };


  async goto() {
    await this.page.goto('https://shopee.co.id/');
  }

  async home(){
    // Click shopee-banner-popup-stateful svg
    await this.page.locator('shopee-banner-popup-stateful svg').click();
  }

  async userGoToLoginPage(){
    // Click text=Log In
    await this.page.locator('text=Log In').click();
    await expect(this.page).toHaveURL('https://shopee.co.id/buyer/login?next=https%3A%2F%2Fshopee.co.id%2F');
  }

  async filledLoginPage(username, password){
    await this.page.locator('[placeholder="No\\. Handphone\\/Username\\/Email"]').fill(username);
    await this.page.locator('[placeholder="Password"]').fill(password);
    await this.page.locator('button:has-text("Log in")').click();
  }

  async validateErrorMessage(){
    await this.page.locator('form div:has-text("Akun dan/atau password Anda salah, silakan coba lagi")').nth(2)
  }
}